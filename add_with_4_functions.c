//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
float input()
{
	float a;
	printf("Enter the number: \n");
	scanf("%f",&a);
	return a;
}

float find_sum(float a,float b)
{
	float sum;
	sum = a+b;
	return sum;
}

void output(float a, float b, float c)
{
	printf("Sum of %f and %f is %f\n ",a,b,c);
}

float main()
{
	float x,y,result;
	x = input();
	y = input();
	result = find_sum(x,y);
	output(x,y,result);
	return 0;
}


