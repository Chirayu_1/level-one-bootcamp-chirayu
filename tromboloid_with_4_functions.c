//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
float input(char b)
{
    float a; 
    printf("Enter the value of %c: ",b);
    scanf("%f",&a);
    return a;
}

float volume(float h,float d,float b)
{
    float vol;
    vol=1.0/3*((h*d*b)+(d/b));
    return vol;
}

void output(float v)
{
    printf("Volume of a tromboloid: %f",v);
}

float main()
{
    float h,d,b,v;
    h=input('h');
    d=input('d');
    b=input('b');
    v=volume(h,d,b);
    output(v);
    return 0;
}